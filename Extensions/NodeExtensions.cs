using Godot;
using NoDontDoIt.DataBinding;

namespace NoDontDoIt.DataBinding.Godot.Extensions
{

    public static class NodeExtensions
    {
        
        private static T FindParent<T>(this Node node)
        {
            for (Node parent = node.GetParent(); parent != null; parent = parent.GetParent())
            {
                if (parent is T castedParent) return castedParent;
            }
            
            return default;
        }

        public static IBindingContext FindBindingContext(this Node node)
        {
            return node.FindParent<IBindingContext>();
        }
        
        public static IRootBindingContext FindRootBindingContext(this Node node)
        {
            return node.FindParent<IRootBindingContext>();
        }
        
    }

}