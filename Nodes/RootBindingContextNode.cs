using System;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using NoDontDoIt.DataBinding.Collections;

namespace NoDontDoIt.DataBinding.Godot.Nodes
{
    
    public class RootBindingContextNode : BindingContextNode, IRootBindingContext
    {

        private ILogger _logger;
        
        private RootBindingContextNode()
            : base(true)
        {
            _logger = NullLogger.Instance;
        }

        public ILogger Logger
        {
            set => _logger = value ?? throw new ArgumentNullException(nameof(value));
        }

        public override void _Process(float delta)
        {
            _context.RebindDirty(_logger);
        }

        public ICommandCollection Commands => ((IRootBindingContext)_context).Commands;
        
        public void ExecuteCommand(string id, object sender)
        {
            ((IRootBindingContext)_context).ExecuteCommand(id, sender);
        }

        public void ExecuteCommand<TValue>(string id, object sender, TValue value)
        {
            ((IRootBindingContext)_context).ExecuteCommand(id, sender, value);
        }
    }
    
}