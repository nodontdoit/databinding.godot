using Godot;
using Microsoft.Extensions.Logging;
using NoDontDoIt.DataBinding.Collections;

namespace NoDontDoIt.DataBinding.Godot.Nodes
{
    
    public class BindingContextNode : Control, IBindingContext
    {

        protected readonly IBindingContext _context;
        
        private BindingContextNode()
            : this(false)
        {
        }

        protected BindingContextNode(bool isRoot, IViewModel viewModel = null)
        {
            _context = isRoot
                ? new RootBindingContext(viewModel)
                : new BindingContext(viewModel);
        }
        
        public IBindingCollection Bindings => _context.Bindings;
        
        public IViewModel ViewModel
        {
            get => _context.ViewModel;
            set => _context.ViewModel = value;
        }

        public void TryMarkDirty(string name) => _context.TryMarkDirty(name);
        public void RebindDirty(ILogger logger) => _context.RebindDirty(logger);
        
    }
    
}