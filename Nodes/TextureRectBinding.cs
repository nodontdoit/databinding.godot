using Godot;
using Microsoft.Extensions.Logging;
using NoDontDoIt.DataBinding.Godot.Extensions;

namespace NoDontDoIt.DataBinding.Godot.Nodes
{
    
    public sealed class TextureRectBinding : TextureRect, IBinding<Texture>
    {

        [Export] private string _propertyName;

        private IBindingContext _context;

        public bool IsDirty { get; private set; }

        public override void _EnterTree()
        {
            SetProcess(false);
            SetPhysicsProcess(false);
            SetProcessInput(false);
            
            _context = this.FindBindingContext();
            _context?.Bindings.Add(_propertyName, this);
        }
        
        public void MarkDirty()
        {
            IsDirty = true;
        }

        public void Bind(Texture value, ILogger logger)
        {
            Texture = value;
            IsDirty = false;
        }

        public override void _ExitTree()
        {
            if (_context == null) return;

            _context.Bindings.Remove(_propertyName, this);
            _context = null;
        }
        
    }
    
}