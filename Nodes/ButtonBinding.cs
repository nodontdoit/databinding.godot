using Godot;
using NoDontDoIt.DataBinding;
using NoDontDoIt.DataBinding.Godot.Extensions;

namespace NoDontDoIt.DataBinding.Godot.Nodes
{
    
    public sealed class ButtonBinding : Button
    {

        [Export] private string _commandId;

        private IRootBindingContext _context;
        
        public override void _EnterTree()
        {
            _context = this.FindRootBindingContext();
        }

        public override void _Pressed()
        {
            _context?.ExecuteCommand(_commandId, this);
        }

        public override void _ExitTree()
        {
            _context = null;
        }
    }
    
}