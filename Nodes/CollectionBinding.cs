using System.Collections.Generic;
using Godot;
using Microsoft.Extensions.Logging;
using NoDontDoIt.DataBinding.Godot.Extensions;

namespace NoDontDoIt.DataBinding.Godot.Nodes
{
    
    public sealed class CollectionBinding : Container, IBinding<ICollection<IViewModel>>
    {
        
        [Export] private string _propertyName;
        [Export] private PackedScene _template; 

        private IBindingContext _context;
        private readonly List<BindingContextNode> _children;

        public bool IsDirty => true;

        private CollectionBinding()
        {
            _children = new List<BindingContextNode>();
        }

        public override void _EnterTree()
        {
            SetProcess(false);
            SetPhysicsProcess(false);
            SetProcessInput(false);
            
            _context = this.FindBindingContext();
            _context?.Bindings.Add(_propertyName, this);
        }
        
        public void MarkDirty()
        {
        }

        public void Bind(ICollection<IViewModel> value, ILogger logger)
        {
            int i;
        
            for (i = _children.Count - 1; i >= value.Count; --i)
            {
                BindingContextNode node = _children[i];
                
                _children.RemoveAt(i);
                
                node.QueueFree();
            }

            for (i = _children.Count; i < value.Count; ++i)
            {
                var node = _template.Instance<BindingContextNode>();

                _children.Add(node);
                
                AddChild(node);
            }

            i = 0;

            foreach (IViewModel item in value)
            {
                BindingContextNode node = _children[i];
                node.ViewModel = item;
                node.RebindDirty(logger);
                ++i;
            }
        }

        public override void _ExitTree()
        {
            if (_context == null) return;

            _context.Bindings.Remove(_propertyName, this);
            _context = null;
        }

    }
    
}